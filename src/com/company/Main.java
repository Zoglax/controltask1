package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i=1, mark, sum=0;

        while (i<=4)
        {
            mark=0;
            do
            {
                System.out.println("Input mark for "+i+" exam:");
                mark=scanner.nextInt();                               //<-- Типичная защита от дурака
            }while (mark>5 || mark<1);                                //<-- (Вернее, проверка на оценки.)
            sum+=mark;
            i++;
        }
        System.out.println("General summ of marks: "+sum);
    }
}
